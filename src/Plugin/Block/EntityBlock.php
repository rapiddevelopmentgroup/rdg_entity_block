<?php

namespace Drupal\rdg_entity_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Display the current entity in a different display mode.
 *
 * @Block(
 *   id = "rdg_entity_block",
 *   admin_label = @Translation("Current entity"),
 *   category = @Translation("Content")
 * )
 */
class EntityBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, EntityDisplayRepositoryInterface $entityDisplayRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Determine what entity type we are referring to.
    $this->entityDisplayRepository = $entityDisplayRepository;

    // Load various utilities related to our entity type.
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->configuration;

    $view_mode = isset($config['view_mode']) ? $config['view_mode'] : NULL;

    $view_modes_info = $this->entityDisplayRepository->getAllViewModes();
    $view_modes = [];
    foreach ($view_modes_info as $entity_type => $mode_list) {
      foreach ($mode_list as $mode_name => $mode_info) {
        $view_modes[$mode_name]['label'] = $mode_info['label'];
        $view_modes[$mode_name]['types'][] = $entity_type;
      }
    }
    $options = array_map(function($x) {
      return $x['label'] . ' (' . implode(', ', $x['types']) . ')';
    }, $view_modes);
    asort($options);

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $options,
      '#default_value' => $view_mode,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Hide default block form fields that are undesired in this case.
    $form['admin_label']['#access'] = FALSE;
    $form['label']['#access'] = FALSE;
    $form['label_display']['#access'] = FALSE;

    // Hide the block title by default.
    $form['label_display']['#value'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * Get the entity being viewed on the current page.
   */
  protected function currentEntity() {
    $routeParameters = \Drupal::routeMatch()->getParameters();
    if ($routeParameters->count() > 0) {
      // Assume the first parameter is the entity type to load (node, user, commerce_product, ...).
      $entityTypeIndicator = \Drupal::routeMatch()->getParameters()->keys()[0];
      $entity = \Drupal::routeMatch()->getParameter($entityTypeIndicator);
      if (is_object($entity) && $entity instanceof ContentEntityInterface) {
        return $entity;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $render = [];

    $render['#cache'] = [
      'contexts' => ['url.path'],
    ];

    $entity = $this->currentEntity();

    if ($entity) {
      $entity_type = $entity->getEntityTypeId();
      $render_controller = $this->entityTypeManager->getViewBuilder($entity_type);
      $view_mode = isset($this->configuration['view_mode']) ? $this->configuration['view_mode'] : 'default';

      $available_view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity_type, $entity->bundle());
      if (isset($available_view_modes[$view_mode])) {
        $render['entity'] = $render_controller->view($entity, $view_mode);
      }
      // Force cache invalidation if the entity is updated.
      $render['#cache']['tags'] = $entity->getCacheTags();
    }

    return $render;
  }

}
